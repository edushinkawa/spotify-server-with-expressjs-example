const express = require("express")
const request = require("request")
const querystring = require("querystring")

const app = express()
const config = {
  client_id: "6274593ac70d4a3dbb2c002f47768450",
  response_type: "code",
  redirect_uri: "http://localhost:3009/callback"
}

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  next()
})

app.get(
  "/",
  (req, res) => {
    const fullUrl = `${req.protocol}://${req.get(
      "host"
    )}${req.originalUrl}callback`
    console.log("fullUrl", fullUrl)
    res.redirect(
      "https://accounts.spotify.com/authorize?" +
        querystring.stringify({
          response_type: "code",
          client_id: config.client_id,
          scope: "user-read-private user-read-email",
          redirect_uri: fullUrl
        })
    )
  },
  (req, res) => {
    res.send("Imgur service error")
  }
)

app.get(
  "/callback",
  (req, res) => {
    const fullUrl = `${req.protocol}://${req.get("host")}/callback`
    const clientUrl =
      `${req.get("host")}` === "localhost:3009"
        ? "localhost:3000"
        : "ifood-test.surge.sh"
    const authOptions = {
      url: "https://accounts.spotify.com/api/token",
      form: {
        code: req.query.code,
        redirect_uri: fullUrl,
        grant_type: "authorization_code"
      },
      headers: {
        Authorization:
          "Basic " +
          new Buffer(
            config.client_id + ":" + "ddbdb398f3e7453abf70e76c7578a633"
          ).toString("base64")
      },
      json: true
    }

    request.post(authOptions, function(error, response, body) {
      // res.json({
      //   access_token: body.access_token,
      //   token_type: body.token_type,
      //   expires_in: body.expires_in,
      //   refresh_token: body.refresh_token,
      //   scope: body.scope
      // })
      res.redirect(
        `http://${clientUrl}/callback?` +
          querystring.stringify({
            access_token: body.access_token,
            token_type: body.token_type,
            expires_in: body.expires_in,
            refresh_token: body.refresh_token,
            scope: body.scope
          })
      )
    })
  },
  (req, res) => {
    res.send("Imgur service error")
  }
)

app.listen(3009, () => {
  console.log("Spotifood app listening on port 3009!")
})
